# README #

The next document contain all necessary steps in order to setup DIIA framework environment.


We develop and deploy the DIIA Framework using Docker containers. Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and ship it all out as one package. 


## Import DIIA Framework code ##

Before we Docker stuff, we will clone the DIIA Framework repository.

```console
git clone https://joseluisvzg@bitbucket.org/diia/rest-api.git
```
## Docker configuration ##

### Configure network ###

In docker a way to communicate services between containers is by the use of a network. The following command create a network called "DIIANetwork".

```console
$ docker network create DIIANetwork
```

### Create Postgres volume ###

In order to persist the data stored by PostgreSQL, we need to create a volume.

```console
$ docker volume create pgdata
```

### Postgres container ###

The next command create a PostgreSQL container, mount the preiously created volume and configure PostgreSQL credentials.

```console
$ docker run --rm --name postgres_server -v <diia_framework_path>/database:/home/ -v pgdata:/var/lib/postgresql/data -e POSTGRES_PASSWORD=mysecretpassword --network DIIANetwork postgres
```

In order to import the DDL into PostgreSQL, we will exec the following command.

```console
$ docker exec -it postgres_server bash -c "cd /home && psql -U postgres -f diia.sql"
```

### Play Framework container ###

In order to create the container, we can use the publicly available image (in our docker hub repository) or by manually building from a Dockerfile file (is a text document that contains all the commands to assemble an image).  

#### Manual image build ####

The next command create the image from the file.

```console
$ docker build -t "play_framework" .
```

#### Download image from hub repository #####

```console
$ docker pull joseluisvzg/play_framework
```

#### Run container ####
```console
$ docker run --rm -it -v "<diia_framework_path>:/home/play/Code" --name DIIA_Framework --network DIIANetwork -p 80:8080 play_framework /opt/activator/bin/activator run -Dhttp.port=8080 -Dhttp.address=0.0.0.0
```

###Test###
```console
$ docker run --rm -it play_framework curl http://localhost:8080/escuelas
```



